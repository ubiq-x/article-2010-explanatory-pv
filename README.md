# Article supplementary materials
This repository contains SAS scripts with statistical analyses having two focal points: [Pre-post knowledge gain](script/kgain/) and [Eye movements](script/em/).  Those analyses were performed for the purpose of the following article:

Loboda T.D. & Brusilovsky P. (2010) User-adaptive explanatory program visualization: Evaluation and insights from eye movements.  _User Modeling and User Adapted Interaction (UMUAI)_, 20(3), 191-226. [[author's copy](umuai-10-loboda.pdf)]


## License
This project is licensed under the [BSD License](LICENSE.md).
