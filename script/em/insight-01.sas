%let DATA_DIR=D:\pitt\projects\wadein\studies\pitt\analysis\sas\et;

ods html style=Default file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out\fix" url=NONE;




proc format;
	value sys_ 0='N' 1='A';
	value sess_ 0='1st' 1='2nd';
	value wm_bin_ 0='low' 1='high';
run;

data Aggr;
	infile "&DATA_DIR\data-aggr-fix.txt" expandtabs firstobs=2;
	input scene$ aoi$ mfd tr t sys sub$ sess fc gt fd pd expl_cnt expl_len expr_cnt expr_len anim_cnt anim_dur wm wm_bin;
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;

data RawFD;
	infile "&DATA_DIR\data-raw-fd-cw.txt" expandtabs firstobs=2;
	input sub$ wm wm_bin sess sys scene$ aoi$ t fd;
	log_fd = log(fd);
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
data RawGT;
	infile "&DATA_DIR\data-raw-gt-cw.txt" expandtabs firstobs=2;
	input sub$ wm wm_bin sess sys scene$ aoi$ t gt;
	log_gt = log(gt);
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;

data RawFDComb;
	infile "&DATA_DIR\data-raw-fd-cw-comb.txt" expandtabs firstobs=2;
	input sub$ wm wm_bin sess sys scene$ aoi$ t dur;
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
data RawGTComb;
	infile "&DATA_DIR\data-raw-gt-cw-comb.txt" expandtabs firstobs=2;
	input sub$ wm wm_bin sess sys scene$ aoi$ t gt;
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;

proc sort data=RawFD; by sub sess t; run;
proc sort data=RawGT; by sub sess t; run;
proc sort data=RawFDComb; by sub sess t; run;
proc sort data=RawGTComb; by sub sess t; run;




ods graphics on / imagefmt=PNG imagename="tmp" reset;
proc univariate data=Aggr normaltest;
	where tr=0 and scene='expr' and aoi='op' and mfd=200;
	var fd;
	class sys;
	*histogram gt / cfill=ligr normal cframe=liy barwidth=8 cv=black;
	*probplot fd / normal(mu=est sigma=est) square name="dur";
	*probplot dur / lognormal(sigma=est) square name="dur";
	*inset mean std / format=6.4;
run;
ods graphics off;


proc capability data=RawFD noprint;
	where fd < 800;
	*spec lsl=200 cleft=orange clsl=black;
	var fd;
	*comphist / class=sys cfill=orange cframetop=ligr cframeside=ligr cframe=ligr;
	histogram fd / lognormal(fill color=paoy theta=est) cfill=paoy cframe=ligr nolegend;
	inset lsl='LSL' lslpct / cfill=blank pos=nw;
	inset lognormal / format=6.2 pos=ne cfill=blank;
run;


ods graphics on / imagefmt=PNG imagename="session-time" reset;
proc sort data=RawGT; by sys; run;
proc boxplot data=RawGT;
	*where dur < 800;
	plot gt*sys / boxstyle=schematicid caxis=black cframe=cxffffff ctext=black cboxes=CX153E7E cboxfill=CX1589FF idcolor=blue idsymbol=circle;
	id sub;
run;
ods graphics off;




ods html close;
