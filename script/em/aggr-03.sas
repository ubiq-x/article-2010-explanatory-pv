%let DATA_DIR=Q:\pitt\projects\wadein\studies\pitt\analysis\sas\et;

ods html style=Default /* style=Journal */ file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out\fix";* (url=NONE);
ods html close;




/* DATA */

proc format;
	value sys_ 0='ctrl' 1='exp';
	value sess_ 1='1st' 2='2nd';
	value wm_bin_ 0='low' 1='high';
run;

data Aggr;
	infile "&DATA_DIR\data-aggr-fix-up-to-1200ms.txt" expandtabs firstobs=2;
	input scene$ aoi$ mfd tr t gt_e gt_s gt_tot sys sub$ sess wm wm_bin pre post gain fc gt fd pd expl_cnt expl_len expr_cnt expr_len anim_cnt anim_dur;
	
	if substr(scene,1,1) = 'e' then sa = cat('s:', aoi);
	else sa = cat('e:', aoi);
	
	gain = post - pre;
	gt_prob = gt/(gt_tot*60);
	
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
proc sort data=Aggr; by tr scene aoi mfd sys wm_bin; run;

data RawFdExpr;
	infile "&DATA_DIR\data-raw-fd-cw-sim-expr.txt" expandtabs firstobs=2;
	input sub$ wm wm_bin sess sys aoi$ t dur;
	if (dur <= 1200);
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;

data RawFdStart;
	infile "&DATA_DIR\data-raw-fd-cw-sim-start.txt" expandtabs firstobs=2;
	input sub$ wm wm_bin sess sys aoi$ t dur;
	if (dur <= 1200);
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;




/* ANALYSIS */

* Descriptives;
proc sort data=Aggr; by mfd sys; run;
goptions reset=all hsize=20in vsize=8in htext=1 ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\box-fd.png";
symbol1 c=blue v=circle h=2;
symbol2 c=red v=plus h=2;
proc boxplot data=Aggr;
	where mfd <> 200 and sa <> 'e:bot' and sa <> 'e:set';
	
	axis1 label=(h=2 'fixation duration [ms]') offset=(2) value=(h=2) w=2;
	axis2 label=(h=2 '') offset=(2) value=(h=2 angle=60) w=2;
	legend1 label=(h=2 'system version') value=(h=2) shape=symbol(3, 1.5);
	
	plot fd*sa(mfd) = sys / symbollegend=legend1 blockpos=2 boxstyle=schematic vaxis=axis1 haxis=axis2 grid cboxfill=white vminor=1 idsymbol=triangle;
run;
proc sort data=Aggr; by tr scene aoi mfd sys wm_bin; run;
quit;


* Some plots;
axis1 label=('Yo') minor=none;
axis2 minor = none;
proc gplot data=Aggr;
	where tr=0 and scene='start' and aoi='txt' and mfd=100;
	plot gt*sys / haxis=axis1 vaxis=axis2;
run;
symbol1 i=none c=blue v=diamond;
symbol2 i=none c=red v=triangle;
proc gplot data=Aggr;
	where tr=0 and scene='start' and aoi='txt' and mfd=100;
	plot fc*sys(sys=0) =1 fc*sys(sys=1) =2 /overlay hminor=0 vminor=0;
run;
* Conclusion: ;


* FR;
%MACRO DO_FC;
	%DO MFD = 100 %TO 300 %BY 200;
		proc glimmix data=Aggr order=data /*plots=(studentpanel(type=blup))*/;
			*nloptions tech=nrridg maxiter=100;
			where tr=0 and scene='start' and aoi='vis-s' and mfd=&MFD;
			offset = log(gt_s);
			class sub sys wm_bin;
			model fc = sys wm_bin sys*wm_bin / d=p link=log s ddfm=kr offset=offset;
			random int / sub=sub gcorr;
			random _residual_;* / grp=sys;
			lsmeans sys wm_bin sys*wm_bin / ilink;
			*lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
			output out=tmp pearson(blup)=res;
		run;
		proc means data=tmp mean std; var res; run;
	%END;
%MEND DO_FC;
%DO_FC;
ods graphics on / imagefmt=PNG imagename="tmp" reset;
ods graphics off;
* Conclusion: ;


proc capability data=RawFdStart graphics noprint;
	histogram log_dur / lognormal;
run;

goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-fd-select-ex.png";
proc capability data=RawFdExpr graphics noprint;
	where aoi='ex';
	
	axis1 label=(h=1 'count') offset=(0 2) value=(h=1) w=2;
	axis2 label=(h=1 'fixation duration (ms)') offset=(2) value=(h=1) w=2;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);
	
	comphistogram dur / class=sys vaxis=axis1 haxis=axis2 grid height=1.5 barlabel=percent vscale=count cfill=white cprop=gray intertile=1 href=300 lhref=2 /*hreflabel='300 ms'*/ midpoints=100 to 1200 by 100 /*kernel(color=black l=4)*/;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=2 cfill=white;
	label sys='Select: ex';
run;
quit;
* Conclusion: ;


* FD - shape insight;
goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\plot-qq-fd.png";
symbol value=plus c=black h=2;
proc univariate data=RawFdStart noprint;
	axis1 label=(h=1 'fixation duration (ms)') offset=(2) value=(h=1) w=2;
	axis2 label=(h=1 'normal quartiles') offset=(2) value=(h=1) w=2;
	where sess=1;
	qqplot dur / normal(mu=est sigma=est color=black l=1 w=2) square cframe=white vaxis=axis1 haxis=axis2;
run;
quit;

proc capability data=Aggr graphics noprint;
	histogram gt_prob / normal;
run;
* Conclusion: ;

* FD histograms;
data RawFdAll;
	set RawFdExpr RawFdStart;
run;

goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-fd-eval-visd.png";
proc capability data=RawFdStart graphics noprint;
	where aoi='vis-d';
	
	axis1 label=(h=1 'count') offset=(0 2) value=(h=1) w=2;
	axis2 label=(h=1 'fixation duration (ms)') offset=(2) value=(h=1) w=2;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);
	
	comphistogram dur / class=wm_bin vaxis=axis1 haxis=axis2 grid height=1.5 barlabel=percent vscale=count cfill=white cprop=gray intertile=1 href=300 lhref=2 /*hreflabel='300 ms'*/ midpoints=100 to 1200 by 100 /*kernel(color=black l=4)*/;
	*comphistogram dur / vaxis=axis1 haxis=axis2 grid height=3 barlabel=percent vscale=count cfill=white cprop=gray intertile=1 href=300 lhref=2 /*hreflabel='300 ms'*/ midpoints=100 to 1200 by 100 /*kernel(color=black l=4)*/;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=2 cfill=white;

	*label sys='System';
	label wm_bin='Working memory index';

	*label sys='Select: ex';
run;
quit;
* Conclusion: ;

* FD (assumed non-normal);
%MACRO DO_FD;
	%DO MFD = 100 %TO 300 %BY 200;
		proc glimmix data=Aggr order=data ic=pq /*plots=(studentpanel(type=blup))*/;
			*nloptions tech=nrridg maxiter=100;
			where tr=0 and scene='expr' and aoi='op' and mfd=&MFD;
			class sub sys wm_bin;
			model fd = sys wm_bin sys*wm_bin / d=logn link=identity s ddfm=kr;
			random int / sub=sub gcorr;
			random _residual_;* / grp=sys;
			lsmeans sys wm_bin sys*wm_bin / ilink;
			*lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
			output out=tmp pearson(blup)=res;
		run;
		/*proc means data=tmp mean std; var res; run;*/
	%END;
%MEND DO_FD;
%DO_FD;
ods graphics on / imagefmt=PNG imagename="tmp" reset;
ods graphics off;


* FD (assumed normal);
%MACRO DO_FD;
	%DO MFD = 100 %TO 300 %BY 200;
		proc mixed data=Aggr covtest method=reml;
			where tr=0 and scene='start' and aoi='vis-d' and mfd=&MFD;
			class sub sys sess wm_bin;
			model fd = sys wm_bin sys*wm_bin / s ddfm=kr;
			random int / sub=sub type=vc;
			repeated sess / sub=sub grp=sys type=vc;
			estimate 'n-a' sys -1 1;
			estimate 'low-high' wm_bin -1 1;
			lsmeans sys wm_bin sys*wm_bin / diff;
		run;
	%END;
%MEND DO_FD;
%DO_FD;
* Conclusion: ;


* Gaze time proportion (or probability) (GT-PROP);
/* http://support.sas.com/documentation/cdl/en/statug/59654/HTML/default/statug_glimmix_sect052.htm */
%MACRO DO_GT_PROP;
	%DO MFD = 100 %TO 300 %BY 200;
		proc glimmix data=Aggr order=data /*plots=(studentpanel(type=blup))*/;
			*nloptions tech=newrap maxiter=100;
			where tr=0 and scene='start' and aoi='vis-s' and mfd=&MFD;
			class sys wm_bin sub;
			offset = log(gt_tot);
			model gt_prob = sys wm_bin sys*wm_bin / d=bin link=logit or ddfm=kr offset=offset s;
			random int / sub=sub gcorr;
			random _residual_;
			lsmeans sys wm_bin sys*wm_bin / ilink;
			*lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
			*estimate 'N vs. A' sys 1 -1 / or cl;
			*estimate 'l vs. h' wm_bin 1 -1 / or cl;
			estimate 'sys (low)'  sys 1 -1 sys*wm_bin [1, 1 1] [-1, 2 1] / cl exp;
			estimate 'sys (high)' sys 1 -1 sys*wm_bin [1, 1 2] [-1, 2 2] / cl exp;
			estimate 'wm_bin (N)' wm_bin 1 -1 sys*wm_bin [1, 1 1] [-1, 1 2] / cl exp;
			estimate 'wm_bin (A)' wm_bin 1 -1 sys*wm_bin [1, 2 1] [-1, 2 2] / cl exp;
			output out=tmp pearson(blup)=res;
		run;
		proc means data=tmp mean std; var res; run;
	%END;
%MEND DO_GT_PROP;
%DO_GT_PROP;
ods graphics on / imagefmt=PNG imagename="tmp" reset;
ods graphics off;
* Conclusion:;


* Multiplicity adjustment -- PROC MULTTEST (raw p-values);
data p;
	input Test$ Raw_P;
	datalines;
	ex_op .0103
	st_txt .3431
	st_visd .0287
	st_visd .0063
;
proc multtest pdata=p holm hoc fdr;
run;
* Conclusion: ;


* Multiplicity adjustment;
* glimmix.pdf, pg: 38, 45;

proc glimmix data=Aggr order=data plots=(studentpanel(type=blup));
	*nloptions tech=nrridg maxiter=100;
	where tr=0 and scene='expr' and aoi='op' and mfd=300;
	offset = log(anim_dur);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / d=p link=log s ddfm=kr offset=offset;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink adjust=simulate(seed=1) cl stepdown adjdfe=row;
	*lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
	*estimate 'N-A' sys -1 1 / adjust=simulate stepdown adjdfe=row;
	*estimate 'low-high' wm_bin -1 1 / adjust=simulate stepdown adjdfe=row;
	output out=tmp pearson(blup)=res;
run;
* Conclusion: AFAIR, I haven't finish the above;


* Inter-endpoint correlation;
data Tmp; set AggrU; fr=fc/expr_cnt; run;
%MACRO DO_CORR;
	%DO MFD = 100 %TO 300 %BY 200;
		proc corr data=Tmp /*pearson spearman*/ fisher nosimple;
			where tr=0 and scene='ex' and aoi='op' and mfd=&MFD;
			var fr fd gt_prob;
		run;
	%END;
%MEND DO_CORR;
%DO_CORR;
* Conclusion: some decent correlations, but what is the implication of 
that is still unclear to me;

