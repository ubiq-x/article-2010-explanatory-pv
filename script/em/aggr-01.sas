* http://groups.google.com/group/comp.soft-sys.sas/browse_thread/thread/3e67fd49cfb451dc;
* The GLIMMIX Procedure: pg 203;


%let DATA_DIR=D:\pitt\projects\wadein\studies\pitt\analysis\sas\et;

ods html style=Default /* style=Journal */ file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out\fix";* (url=NONE);
ods noproctitle;




proc format;
	value sys_ 0='N' 1='A';
	value sess_ 0='1st' 1='2nd';
	value wm_bin_ 0='low' 1='high';
run;
data D;
	infile "&DATA_DIR\data-fix.txt" expandtabs firstobs=2;
	input scene$ aoi$ mfd tr t sys sub$ sess fc gt fd pd expl_cnt expl_len expr_cnt expr_len anim_cnt anim_dur wm wm_bin;
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
proc sort data=D; by tr scene aoi mfd sys sub; run;




/* --- EXPR: OP --- */

title1 "EXPR: OP";

title2 "FC (rate): t=full, mdf=50";
ods graphics on / imagefmt=PNG imagename="fc-expr-op-tr=0-mfd=50" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	nloptions tech=nrridg maxiter=100;
	where tr=0 and scene='expr' and aoi='op' and mfd=50;
	log_expr_cnt = log(expr_cnt);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat offset=log_expr_cnt;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=50";
proc glimmix data=D;
	where tr=0 and scene='expr' and aoi='op' and mfd=50;
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;

title2 "FC (rate): t=full, mdf=200";
ods graphics on / imagefmt=PNG imagename="fc-expr-op-tr=0-mfd=200" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	nloptions tech=nrridg maxiter=100;
	where tr=0 and scene='expr' and aoi='op' and mfd=200;
	log_expr_cnt = log(expr_cnt);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat offset=log_expr_cnt;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=200";
proc glimmix data=D;
	where tr=0 and scene='expr' and aoi='op' and mfd=200;
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;


/* --- START: VAL-AN --- */

title1 "START: VAL-AN";

title2 "FC (rate): t=full, mdf=50";
ods graphics on / imagefmt=PNG imagename="fc-start-val-an-tr=0-mfd=50" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	where tr=0 and scene='start' and aoi='val-an' and mfd=50;
	log_anim_dur = log(anim_dur);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat offset=log_anim_dur;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=50";
proc glimmix data=D;
	where tr=0 and scene='start' and aoi='val-an' and mfd=50;
	class sub sys wm_bin sess;
	model fc = sys wm_bin sys*wm_bin sess sys*sess / dist=p s ddfm=sat;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;

title2 "FC (rate): t=full, mdf=200";
ods graphics on / imagefmt=PNG imagename="fc-start-val-an-tr=0-mfd=200" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	where tr=0 and scene='start' and aoi='val-an' and mfd=200;
	log_anim_dur = log(anim_dur);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat offset=log_anim_dur;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=200";
proc glimmix data=D;
	where tr=0 and scene='start' and aoi='val-an' and mfd=200;
	title "FC rate: expr - op tr=0, mfd=200ms";
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;


/* --- START: C-VAR --- */

title1 "START: C-VAR";

title2 "FC (rate): t=full, mdf=50";
ods graphics on / imagefmt=PNG imagename="fc-start-c-var-tr=0-mfd=50" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	nloptions tech=nrridg maxiter=100;
	where tr=0 and scene='start' and aoi='c-var' and mfd=50;
	log_anim_dur = log(anim_dur);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat offset=log_anim_dur;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=50";
proc glimmix data=D;
	where tr=0 and scene='start' and aoi='c-var' and mfd=50;
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;

title2 "FC (rate): t=full, mdf=200";
ods graphics on / imagefmt=PNG imagename="fc-start-c-var-tr=0-mfd=200" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	where tr=0 and scene='start' and aoi='c-var' and mfd=200;
	log_anim_dur = log(anim_dur);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat offset=log_anim_dur;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=200";
proc glimmix data=D;
	where tr=0 and scene='start' and aoi='c-var' and mfd=200;
	title "FC rate: expr - op tr=0, mfd=200ms";
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=nb s ddfm=sat;
	random int / sub=sub gcorr;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;


/* --- START: TXT --- */

title1 "START: TXT";

title2 "FC (rate): t=full, mdf=50";
ods graphics on / imagefmt=PNG imagename="fc-start-txt-tr=0-mfd=50" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	where tr=0 and scene='start' and aoi='txt' and mfd=50;
	log_expl_len = log(expl_len);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat offset=log_expl_len;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=50";
proc glimmix data=D;
	where tr=0 and scene='start' and aoi='txt' and mfd=50;
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;

title2 "FC (rate): t=full, mdf=200";
ods graphics on / imagefmt=PNG imagename="fc-start-txt-tr=0-mfd=200" reset;
proc glimmix data=D order=data plots=(studentpanel(type=blup));
	where tr=0 and scene='start' and aoi='txt' and mfd=200;
	log_anim_dur = log(anim_dur);
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat offset=log_anim_dur;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
	lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
run;
ods graphics off;

title2 "FC: t=full, mdf=200";
proc glimmix data=D;
	where tr=0 and scene='start' and aoi='txt' and mfd=200;
	title "FC rate: expr - op tr=0, mfd=200ms";
	class sub sys wm_bin;
	model fc = sys wm_bin sys*wm_bin / dist=p s ddfm=sat;
	random int / sub=sub gcorr;
	random _residual_ / grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink cl;
run;




ods html close;
