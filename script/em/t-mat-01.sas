%let DATA_DIR=Q:\pitt\projects\wadein\studies\pitt\analysis\sas\et;

ods latex style=Journal file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out\t-mat" (url=NONE);
ods latex close;




/* --- DATA --- */

proc format;
	value sys_ 0='ctrl' 1='exp';
	value sess_ 1='1st' 2='2nd';
	value wm_bin_ 0='low' 1='high';
run;
data D;
	infile "&DATA_DIR\data-aggr-t-mat.txt" expandtabs firstobs=2;
	input mfd tr t gt_e gt_s gt_tot sys sub$ sess wm wm_bin expl_cnt expl_len expr_cnt expr_len anim_cnt anim_dur e__ex_op s__viss_op s__visd_op s__txt_op s__viss_txt s__visd_txt s__viss_visd s__viss_viss s__visd_visd s__txt_txt pre post shift_grp;
	gain = post - pre;
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
proc sort data=D; by tr mfd sys sub; run;




/* --- ANALYSIS --- */

ods graphics on / imagefmt=PNG imagename="e-ex-op-tr=0-mfd=50" reset;


* Investigating the mount of gaze shifts;
proc glimmix data=D order=data /*plots=(studentpanel(type=blup))*/;
	*nloptions tech=nrridg maxiter=100;
	offset = log(expl_len);
	class sub sys wm_bin;
	model s__txt_op = sys wm_bin sys*wm_bin / dist=p s ddfm=kr offset=offset;
	random int / sub=sub gcorr;
	random _residual_ ;*/ grp=sys;
	lsmeans sys wm_bin sys*wm_bin / ilink;
	*lsmeans sys*wm_bin / slicediff=(sys wm_bin) plots=(diffplot);
	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;


* Investigating the effect of vis-d - txt gaze shifts on knowledge gain;
proc mixed data=D;
	class sub sess shift_grp;
	model gain = sess shift_grp wm / ddfm=kr residual outp=Aggr;
	random int / sub=sub g gcorr;
	repeated sess / sub=sub type=vc r rcorr;
	lsmeans sess shift_grp;
run;




ods graphics off;


