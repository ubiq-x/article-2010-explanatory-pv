ods latex style=Journal file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out" (url=NONE);
ods latex close;

%let DATA_DIR=D:\pitt\projects\wadein\studies\pitt\analysis\sas\et;


/* --- DATA ---*/

proc format;
	value sys_ 0='N' 1='A';
	value sess_ 1='1st' 2='2nd';
	value wm_bin_ 0='low' 1='high';
run;
data Aggr;
	infile "&DATA_DIR\data-aggr-fix.txt" expandtabs firstobs=2;
	input scene$ aoi$ mfd tr t gt_e gt_s gt_tot sys sub$ sess wm wm_bin pre post gain fc gt fd pd expl_cnt expl_len expr_cnt expr_len anim_cnt anim_dur;
	gain = post - pre;
	gt_prob = gt/(gt_tot*60);
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
proc sort data=Aggr; by tr scene aoi mfd sub sys; run;


/* --- ANALYSIS ---*/

* Checking correlation between variables;
proc corr data=Aggr pearson;
	where tr=0 and scene='start' and aoi='vis-d' and mfd=300;
	var gain wm gt_prob;
run;


* Fitting the gain model;
ods html;
ods graphics on;
proc mixed data=Aggr covtest boxplot;
	where tr=0 and scene='start' and aoi='vis-d' and mfd=300;
	class sub sys sess;
	model gain = sess wm gt_prob wm*gt_prob / s ddfm=kr residual outp=Aggr;
	random int / sub=sub type=vc;
	repeated sess / sub=sub grp=sys type=vc r;
run;
ods graphics off;
ods html close;


* Plotting: gain(pred)*wm;
goptions reset=global gunit=pct border cback=white ftext=swiss htext=2.5;
proc gplot data=Aggr;
	where tr=0 and scene='start' and aoi='vis-d' and mfd=300;
	symbol1 interpol=rl value=circle height=2 cv=black ci=black co=black width=2 line=1;
	symbol2 interpol=rl value=plus height=2 cv=black ci=black co=black width=2 line=2;
	plot gain*wm = sess / haxis=0.6 to 1 by 0.1 vaxis=10 to 25 by 5 hminor=1;
run;


* Plotting: gain(pred)*wm;
goptions reset=global gunit=pct border cback=white ftext=swiss htext=2.5;
proc gplot data=Aggr;
	where tr=0 and scene='start' and aoi='vis-d' and mfd=300;
	symbol1 interpol=rl value=circle height=2 cv=black ci=black co=black width=2 line=1;
	symbol2 interpol=rl value=plus height=2 cv=black ci=black co=black width=2 line=2;
	plot pred*wm = sess / haxis=0.6 to 1 by 0.1 vaxis=10 to 25 by 5 hminor=1;
run;


* Plotting: gain*gt_prob;
goptions reset=global gunit=pct border cback=white ftext=swiss htext=2.5;
proc gplot data=Aggr;
	where tr=0 and scene='start' and aoi='vis-d' and mfd=300;
	axis1 label='gain - predicted';
	symbol1 interpol=rl value=circle height=2 cv=black ci=black co=black width=2 line=1;
	symbol2 interpol=rl value=plus height=2 cv=black ci=black co=black width=2 line=2;
	plot gain*gt_prob = sess / haxis=0 to 0.25 by 0.1 vaxis=10 to 25 by 5 hminor=1;
run;


* Plotting: gain(pred)*gt_prob;
goptions reset=global gunit=pct border cback=white ftext=swiss htext=2.5;
proc gplot data=Aggr;
	where tr=0 and scene='start' and aoi='vis-d' and mfd=300;
	axis1 label='gain - predicted';
	symbol1 interpol=rl value=circle height=2 cv=black ci=black co=black width=2 line=1;
	symbol2 interpol=rl value=plus height=2 cv=black ci=black co=black width=2 line=2;
	plot pred*gt_prob = sess / haxis=0 to 0.25 by 0.1 vaxis=10 to 25 by 5 hminor=1;
run;
