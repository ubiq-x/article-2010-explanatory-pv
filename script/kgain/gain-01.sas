%let DATA_DIR=Q:\pitt\projects\wadein\studies\pitt\analysis\sas\k-gain;

ods latex style=Journal file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out" (url=NONE);
ods latex close;




/* --- DATA --- */

proc format;
	value sys_ 0='N' 1='A';
	value sess_ 1='1st' 2='2nd';
	value wm_bin_ 0='low' 1='high';
run;

data AggrU;
	infile "&DATA_DIR\aggr-u.txt" expandtabs firstobs=2;
	input sub$ sys sess pre post t wm wm_bin evt_cnt_all evt_cnt_5 env_cnt_8 sys_sess$;
	gain = post - pre;
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
proc sort data=AggrU; by sub sys; run;


data TTest;
	infile "&DATA_DIR\ttest.txt" expandtabs firstobs=2;
	input sub pre_sys0 pre_sys1 post_sys0 post_sys1 gain_sys0 gain_sys1 pre_oset1 pre_oset2 post_oset1 post_oset2 gain_oset1 gain_oset2;
	if (sub ^= 6);
run;
proc sort data=TTest; by sub; run;


data Sub;
	infile "&DATA_DIR\sub.txt" expandtabs firstobs=2;
	input sub pre post gain wm math;
	sub = sub - 4;
run;
proc sort data=Sub; by sub; run;


data ModsSetSize;
	infile "&DATA_DIR\mods-set-size.txt" expandtabs firstobs=2;
	input size partial perfect;  *partial and perfect refer to recall;
run;
proc sort data=ModsSetSize; by size; run;


data ModsSerialPos;
	infile "&DATA_DIR\mods-serial-pos.txt" expandtabs firstobs=2;
	input pos size3 size4 size5 size6;
run;
proc sort data=ModsSerialPos; by pos; run;




/* --- ANALYSIS --- */

* Checking for outliers;
proc reg data=Sub noprint;
	model gain = sub;
	output out=tmp cookd=cookd;
run;
goptions reset=all hsize=20in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\plot-cooks-d.png";
proc gplot data=tmp;
	axis1 label=(h=2 angle=90 'Cook''s D') offset=(2) value=(h=2) w=2;
	axis2 label=(h=2 'subject') minor=none offset=(2) value=(h=2) w=2;
	symbol1 i=none ci=black v=circle l=1 c=black h=2;
	
	plot cookd * sub / vaxis=axis1 haxis=axis2;
run;
quit;
* Conclusion: Second subject is an outlier (Cook's d very high). It's consistent 
with the fact that they didn't learn anything in either of the sessions;


* Checking the relationship between working memory index and math test results;
data tmp;
	set Sub;
	if (sub ^= 2);
run;
proc corr data=tmp;
	var wm math;
run;
goptions reset=all hsize=20in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\plot-wm-math.png";
proc gplot data=tmp;
	axis1 label=none offset=(2) value=(h=2) w=2;
	axis2 label=(h=2 'subject') minor=none offset=(2) value=(h=2) w=2 order=(1,3,4,5,6,7,8,9,10,11,12,13,14,15);
	symbol1 i=none ci=black v=circle l=1 c=black h=2;
	symbol2 i=none ci=black v=plus l=2 c=black h=2;
	legend1 label=none cborder=black down=2 position=(bottom right inside) shape=symbol(4.5,1.5) value=(justify=l h=2 'wm index' 'math test') offset=(-1 1);
	note h=1.5 move=(7,34.2) 'median wm index';
	
	plot wm*sub =1 math*sub =2 / overlay legend=legend1 vaxis=axis1 haxis=axis2 vref=0.78 lvref=2;
run;
quit;
* Conclusion: corr(wm,math)=0.56736;


* Checking the MODS task results;
goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\mods-mem-set-size.png";
proc gplot data=ModsSetSize;
	axis1 label=(h=2 angle=90 'correct recall proportion') offset=(2) value=(h=2) w=2;
	axis2 label=(h=2 'memory set size') minor=none offset=(2) value=(h=2) w=2;
	symbol1 i=join ci=black v=circle l=1 c=black h=2 w=2;
	symbol2 i=join ci=black v=triangle l=2 c=black h=2 w=2;
	legend1 label=none cborder=black down=2 position=(top right inside) shape=symbol(7.5,1.5) value=(justify=l h=2 'partial' 'perfect') offset=(-2 -1);
	
	plot partial*size =1 perfect*size=2 / overlay legend=legend1 vaxis=axis1 haxis=axis2;
run;
quit;
goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\mods-serial-pos.png";
proc gplot data=ModsSerialPos;
	axis1 label=(h=2 angle=90 'correct partial recall proportion') offset=(2) value=(h=2) w=2 order=(0 to 1 by .1);
	axis2 label=(h=2 'serial position') minor=none offset=(2) value=(h=2) w=2;
	symbol1 i=join ci=black v=diamond l=1 c=black h=2 w=2;
	symbol2 i=join ci=black v=square l=2 c=black h=2 w=2;
	symbol3 i=join ci=black v=triangle l=3 c=black h=2 w=2;
	symbol4 i=join ci=black v=circle l=4 c=black h=2 w=2;
	legend1 label=none cborder=black down=4 position=(bottom right inside) shape=symbol(7.5,1.5) value=(justify=l h=2 'set size = 3' 'set size = 4' 'set size = 5' 'set size = 6') offset=(-2 1);
	
	plot size3*pos =1 size4*pos =2 size5*pos =3 size6*pos =4 / overlay legend=legend1 vaxis=axis1 haxis=axis2;
run;
quit;
* Conclusion: ;


* Checking normality of the DV (gain) (for ANCOVA);
goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\plot-qq-gain.png";
symbol value=plus c=black h=2;
proc univariate data=AggrU noprint;
	axis1 label=(h=1 'gain score') offset=(2) value=(h=1) w=2;
	axis2 label=(h=1 'normal quartiles') offset=(2) value=(h=1) w=2;
	where sess=1;
	qqplot gain / normal(mu=est sigma=est color=black l=1 w=2) square cframe=white vaxis=axis1 haxis=axis2;
run;
quit;
* Conclusion: The fit is reasonable, even though we will include a random variable 
later on and repeat the assessment for the conditional studentized residuals;


* Checking the DV-Covariate relationships;
data TmpSys0; set AggrU; where sys=0; gain0=gain; wm0=wm; keep sub gain0 wm0; run;
data TmpSys1; set AggrU; where sys=1; gain1=gain; wm1=wm; keep sub gain1 wm1; run;
data TmpGainWm; merge TmpSys0 TmpSys1; by sub; run;
proc print data=TmpGainWm; run;

goptions reset=all hsize=20in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\scatter-gain-wm.png";
proc gplot data=TmpGainWm;
	symbol1 i=rl ci=black v=circle c=black h=2;
	symbol2 i=rl ci=black v=plus l=2 c=black h=2;
	axis1 label=(h=2 angle=90 'gain score') offset=(2) value=(h=2) w=2;
	axis2 label=(h=2 'working memory index') offset=(2) value=(h=2) w=2;
	legend1 label=none cborder=black down=2 position=(bottom right inside) shape=symbol(4.5,1.5) value=(justify=l h=2 'control' 'exp') offset=(-1 1);
	*note h=1.5 move=(7,34.2) 'median wm index';
	
	plot gain0*wm0 =1 gain1*wm1 =2 / overlay legend=legend1 vaxis=axis1 haxis=axis2 /*vref=0.78 lvref=2*/;
run;
quit;
* Conclusion: Shouldn't be looking at that too much since we will
be fitting a mixed model. Later on we should investigate the 
conditional studentized residuals;


* Checking the total session time difference;
proc ttest data=Aggr;
	paired t1*t2;
run;
* Conclusion: none;


* Checking the homogeity of groups created by randomization (i.e. no 
difference w.r.t. the pre-score);
proc ttest data=TTest;
	paired pre_sys1*pre_sys0;
run;

proc ttest data=TTest;
	paired pre_oset1*post_oset1;
run;
* Conclusion: no evidence of difference;

* Checking the homogeneity of variances (for ANCOVA) assumption;
proc glm data=AggrU;
	class sys;
	model gain = sys;
	means sys / hovtest=levene(type=abs) welch;
run;
* Conclusion: Levene's test for homogeneity of gain variance indicates 
that there is no evidence against the equal variance hypothesis (p-value = 
0.9812).;


* Checking the homogeneity of variances (for ANCOVA) assumption;
ods html style=Journal;
ods graphics on;
proc mixed data=AggrU boxplot;
	class sys_sess;
	model gain = sys_sess;
run;
ods graphics off;
ods html close;
* Conclusion: sys_sess - p=0.0228 (forgot what this means, so no conclusions);


* Levene's test for homogeneity of gain variance carried out as an ANOVA on 
absolute residuals;
data tmp; 
	set Aggr;
	AbsResid = abs(Resid);
run;
proc glm data=tmp;
	class sys sess;
	model AbsResid = sys|sess;
run;
* Conclusion: no signs of heterogeneity of variances;

* Checking for the homonegeneity of regressions assumption (for ANCOVA);
* 1. Fitting factorial effects for both intercepts and slopes model;
proc mixed data=AggrU;
	class sub sys sess;
	model gain = sys sess sys*sess wm wm*sys wm*sess wm*sys*sess / ddfm=kr;
	random sub;
	repeated sess / sub=sub grp=sys type=vc;
run;
* Conclusion: p-value for wm*sys*sess = 0.2358 -- we can remove it;


* 2. Fitting factorial effects for intercepts and main effects for slopes model;
proc mixed data=AggrU;
	class sub sys sess;
	model gain = sys sess sys*sess wm wm*sys wm*sess / ddfm=kr;
	random sub;
	repeated sess / sub=sub grp=sys type=vc;
run;
* Conclusion: p-value for wm*sys*sess = 0.6005 -- we can remove it;


* 3. Fitting factorial effects for intercepts and the slope as a function of sys model;
proc mixed data=AggrU;
	class sub sys sess;
	model gain = sys sess sys*sess wm wm*sys / ddfm=kr;
	random sub;
	repeated sess / sub=sub grp=sys type=vc;
run;
* Conclusion: p-value for wm*sys = 0.8082 -- we can remove it and fit 
common slope model instead;


* 4. Fitting a common slope model;
proc mixed data=AggrU;
	*where sub ^= "s05";
	class sub sys sess;
	model gain = sys sess sys*sess wm / ddfm=kr;
	random sub;
	repeated sess / sub=sub grp=sys type=vc;
run;
* Conclusion: p-value for wm = 0.0340 indicating that the covariate is 
needed in the model. There is no sys*sess interaction;


* 5. Refitting the previous model w/o the remaining interaction;
ods html style=Journal;
ods graphics on;
proc mixed data=AggrU;
	class sub sys sess;
	model gain = sys sess wm / ddfm=kr residual outp=Aggr;
	random int / sub=sub g gcorr;
	repeated sess / sub=sub type=vc r rcorr;
	lsmeans sys sess;
run;
ods graphics off;
ods html close;
* Conclusion: none;


* Checking descriptives;
proc univariate data=Aggr;
	*class sess;
	var pre post;
run;
* Conclusion: none;


* Checking for learning with the system (post-pre difference);
proc ttest data=Aggr;
	paired pre*post;
run;
* Conclusion: none;
