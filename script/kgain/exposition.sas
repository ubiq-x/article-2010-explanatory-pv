ods latex style=Journal file="body.htm" contents="contents.htm" frame="frame.htm" path="&DATA_DIR\out" (url=NONE);
ods latex close;

%let DATA_DIR=D:\pitt\projects\wadein\studies\pitt\analysis\sas\k-gain;


/* --- DATA --- */

proc format;
	value sys_ 0='N' 1='A';
	value sess_ 1='1st' 2='2nd';
	value wm_bin_ 0='low' 1='high';
run;

data Aggr;
	infile "&DATA_DIR\aggr-u.txt" expandtabs firstobs=2;
	input sub$ sys sess pre post t wm wm_bin evt_cnt_all evt_cnt_5 env_cnt_8;
	gain = post - pre;
	evt_rate_5 = evt_cnt_5 / (t / 60);
	format sys sys_. sess sess_. wm_bin wm_bin_.;
run;
proc sort data=Aggr; by sub sys; run;


/* --- ANALYSIS --- */

* Checking descriptives;
proc univariate data=Aggr;
	class sess;
	var evt_rate_5;
run;


* Fitting the model;
ods html style=Journal;
ods graphics on / imagefmt=PNG imagename="evt-cnt-5-sys" reset;
proc glimmix data=Aggr order=data plots=(studentpanel(type=blup));
	class sub sys sess;
	tmin = t / 60;
	offset = log(tmin);
	model evt_cnt_5 = sys sess wm/ d=p s ddfm=kr offset=offset;
	random int / sub=sub gcorr type=vc g gcorr;
	*random _residual_ / grp=sys;
	lsmeans sys sess / ilink;
	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;
ods graphics off;
ods html close;
